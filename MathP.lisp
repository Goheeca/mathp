#|;;;; MathP - Maths in Lisp!
Version 1.11

Copyright (c) 2012, Jonathan Johansen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#

(defpackage #:mathp
    (:use "COMMON-LISP")
    (:export #:mathp #:fn #:square #:matrix #:square-matrix #:define-op #:define-function))

(in-package #:mathp)

;;; MathP contains a lexer and a parser. Use #M to lex maths, and (mathp ...) to parse maths.
;;; I've tried to make it extensible, but YMMV.


#|;;; Idea for lexing maths and producing an AST:
Make closures, named detectors that detect certain objects when called
with successive characters from a stream. When a detection is made, the
detector returns an accumulator function/closure that is called with the
stream and reads from it and returns the 'accumulated' object. The
accumulator is responsible for leaving the stream in the correct state
afterwards. Detectors should not normally match on escaped characters.

(defun prototype-detector (possibly-escaped-char)
    (values accumulator-on-match matched-chars-so-far))

(defun prototype-accumulator (stream &optional overshoot-detectors)
    parsable-object)
|#

;; Utilities

(defmacro aif (test-form then-form &optional else-form)
    `(let ((it ,test-form)) (if it ,then-form ,else-form)))

(defmacro fn (args &body body)
    (let (arg-list ignores)
        (loop for arg in args
            if (string= (symbol-name arg) "_")
            do (let ((sym (gensym)))
                (push sym ignores)
                (push sym arg-list))
            else do (push arg arg-list))
        `(lambda ,(nreverse arg-list) (declare (ignore ,@ignores)) ,@body)))

(defmacro iferror (try fallback) `(handler-case ,try (error () ,fallback)))

(defun square (x) (* x x))

(defun matrix (n m func) "Constructs a matrix from sizes and a function"
    (let ((matrix (make-array (list n m))))
        (loop for i from 0 below n
            do (loop for j from 0 below m
                do (setf (aref matrix i j) (funcall func i j))))
        matrix))

(defun square-matrix (&rest args) "Constructs a square matrix from args if there's a square
number of them, or if two terms, it must be a size and a function of two arguments."
    (let* ((len (length args))
           (n (isqrt len)))
        (cond ((= len (square n))
                (let* ((matrix (make-array (list n n)))
                       (linear (make-array len :displaced-to matrix)))
                    (loop for i from 0 and x in args do (setf (aref linear i) x))
                    matrix))
              ((= len 2)
                (matrix (car args) (car args) (cadr args)))
              (t (error "Invalid number of arguments to square-matrix.")))))


(defparameter *escape-character* #\\ "The escape character used to indicate that the following character is to be treated like a normal character.")

(defun escaping-read-char (stream)
    "Returns the next character from stream, in the CAR of a cons if it was
escaped (with the *escape-character*)."
    (aif (read-char stream nil nil t)
        (if (char= it *escape-character*)
            (cons (read-char stream nil nil t) nil)
            it)
        nil))

(defun raw-char (character) "Unpacks a possibly escaped character to just the raw character."
    (if (consp character) (car character) character))

(defun escaping-unread-char (character stream)
    "Unreads a possibly escaped character to the stream. An escaped character is denoted by
a cons with the character in the CAR."
    (if (consp character)
        (progn (unread-char (car character) stream)
               (unread-char *escape-character* stream))
        (unread-char character stream)))

(defun make-detector (target &optional result)
    "Returns a closure that when repeatedly called on characters, will return
result (or the original target if not supplied) when (and if) it
matches the target, and a second value when a match is in progress.
If target is a list of characters, result must be a function. It will be
called on the matching character and it's result returned."
    (let ((result (or result target)))
        (cond
            ((or (characterp target) (and (stringp target) (= 1 (length target))))
                (let ((char-target (if (characterp target) target (char target 0))))
                    (fn (character)
                        (and (characterp character)
                            (if (char= character char-target)
                                (values result 1)
                                (values nil nil))))))
            ((and (consp target) (every #'characterp target));; List of characters
                (fn (character)
                    (and (characterp character)
                        (if (member character target :test #'char=)
                            (values (funcall result character) 1)
                            (values nil nil)))))
            ((stringp target)
                (let ((len (length target))
                      (in-progress '())); List of integers showing progress of matches
                    (fn (c) "Produced by make-detector. Call with characters to try to match. If
a match occurs, or a partial match is in progress, the number of matched
characters is returned as a second value.
Pass a number to clear (internal) partial matches that long or longer.
Pass nil or a cons to clear all (internal) partial matches."
                        (let ((matched nil))
                            (setf in-progress
                                (cond ((listp c) nil);; Escaped or nil.
                                      ((integerp c)
                                        (remove-if (lambda (n) (>= n c)) in-progress))
                                      (t (loop for i in (cons 0 in-progress)
                                            when (char= c (char target i))
                                            if (= (1+ i) len)
                                            do (setf matched t)
                                            else collect (1+ i)))))
                            (if matched (values result len)
                                        (values nil (car (last in-progress)))))))))))
#|
(defparameter sm (make-detector "hello"))
(mapcar sm '(#\h #\e #\l #\l #\o)); Should match!
(mapcar sm '(#\h #\e #\l #\p #\o)); Should not match, as p breaks match.
(mapcar sm '(#\h #\e #\l nil #\l #\o)); Should not match because nil resets it.
(mapcar sm '(#\h #\e #\l 3 #\l #\o)); Should not match because 3 clears partial matches of 3 chars or longer.

(defparameter sm (make-detector "hoho"))
(map 'list sm "hey hohoho!"); => (NIL NIL NIL NIL NIL NIL NIL T NIL T NIL)
|#

(defun winning-detector (stream detectors &optional full-reset)
    "Returns the winning detector for this stream. Leaves the stream with the
chars just after the winning detector triggered, or all if full-reset is non-nil."
    (loop with winner = nil
           and goings = detectors
           and read-in = '()
          for char = (escaping-read-char stream)
        do (push char read-in)
        do (setf goings (remove-if; the detectors (d) down to the ones that are still "going".
                (fn (d)
                    (multiple-value-bind (result going) (funcall d char)
                        (when result (setf winner result); Last match is winner
                                     (unless full-reset (setf read-in nil)))
                        (or (not going) result)))
                goings))
        while goings; Keep going until all have been eliminated, as a match is also 'eliminated'.
        finally (progn
            (loop for c in read-in do (escaping-unread-char c stream))
            (loop for d in detectors do (funcall d nil)); Clear all detectors' internal state.
            (return winner))))

#|
Read character from stream and call each detector with char. If any start matching, take ones
that started and return the result of the last one to successfully match.

(defun winner-then-read (string detectors)
    (with-input-from-string (in string)
        (values (winning-detector in detectors)
                (escaping-read-char in))))

(winner-then-read "+.abc\\d\\ef" (mapcar #'make-detector '("+" "+." ".+") '(plus plus-dot dot-plus)))
(winner-then-read " +.abc\\d\\ef" (mapcar #'make-detector '("+" "+." ".+")))
(winner-then-read "+.abc\\d\\ef" (mapcar #'make-detector '("+" "-" "*" "/") '(plus minus times divide)))
(winner-then-read "/.abc\\d\\ef" (mapcar #'make-detector '("+" "-" "*" "/") '(plus minus times divide)))
(winner-then-read "+plus* oh ho!" (mapcar #'make-detector '("+" "+plus+" "+plus-")
                                                  '(plus plus-plus-plus plus-plus-minus)))
(winner-then-read "+plus* oh ho!" (mapcar #'make-detector '("+plus/" "+plus+" "+plus-")))
(winner-then-read "#+cf" (mapcar #'make-detector '("+" "-" "*" "/")))
(winner-then-read "\\+cf" (mapcar #'make-detector '("+" "-" "*" "/")))
|#

(defun convert-identifier (reverse-char-list)
    "Read in an object and convert camel case names to lisp style hyphenated names."
    (coerce (let ((chars (when reverse-char-list (list (car reverse-char-list))))
                  (later (car reverse-char-list)))
                (dolist (c (cdr reverse-char-list) chars)
                    (when (and (lower-case-p c) (upper-case-p later))
                        (push #\- chars))
                    (push c chars)
                    (setf later c)))
            'string))
; (convert-identifier '(#\a #\b #\c #\d))
; (convert-identifier '(#\a #\B #\c #\d))
; (convert-identifier '())
; (convert-identifier '(#\d #\e #\e #\F #\m #\r #\o #\f))

(defun read-until-string (stream string) "Reads the characters up till the string is found (or end of file)."
    (loop with ender = (make-detector string)
           and output = '()
          for c = (read-char stream nil nil)
          for end = (funcall ender c)
        while (and c (not end))
        do (push c output)
        finally (return (coerce (nreverse (nthcdr (if end (1- (length string)) 0) output)) 'string))))
; (with-input-from-string (in "stuff then end it") (read-until-string in "end"))
; (with-input-from-string (in "stuff then don't stop it") (read-until-string in "end"))

(defparameter *detectors* (make-hash-table))
(defun define-lexable (name detector)
    (setf (gethash name *detectors*) detector)
    name)

(defun detector (name) (gethash name *detectors*))
(defun detectors () (let (dets) (maphash (fn (_ v) (push v dets)) *detectors*) dets))
(defun detector-name (detector)
    (maphash (fn (k v) (when (eq v detector) (return-from detector-name k))) *detectors*))

;; String lexing:
(defun string-accumulator (stream &optional overshoot-detectors) (declare (ignore overshoot-detectors))
    (loop with contents = '()
        for c = (escaping-read-char stream)
        until (or (not c) (and (characterp c) (char= c #\")));; Finish string only when end of input or unescaped double-quote.
        do (push (raw-char c) contents)
        finally (return (coerce (nreverse contents) 'string))))

(define-lexable :string (make-detector #\" #'string-accumulator))
;; Slash may be the escape character, so we have to be clever to make #\ work in both cases...
(defun figure-out-char (stream &optional overshoot-detectors starting-letter)
    (loop with detectors = (detectors)
           and output = (if starting-letter (list starting-letter) '())
        repeat 200;; Safety limit on maximum character name length
        until (or (winning-detector stream overshoot-detectors t)
                  (winning-detector stream detectors t);; Reset the stream on any detection, ready for next token after character.
                  (member (peek-char nil stream t t t) '(#\Space #\Tab #\Newline)))
        ;; Using escaping-read-char above means that you can escape special characters in character names.|#
        do (push (raw-char (escaping-read-char stream)) output)
        finally (return
            (if (cdr output)
                (let* ((name (string-upcase (convert-identifier output)))
                       (character (name-char name)))
                    (unless (characterp character) (error "Invalid character named ~S." name))
                    character)
                (car output)))))

(if (char= *escape-character* #\\)
    (define-lexable :character
        (let ((last nil))
            (fn (c)
                (cond ((equal c #\#) (setf last c) (values nil 1))
                      ((and (equal last #\#) (consp c))
                        (setf last nil)
                        (values (fn (s &optional overshoot-detectors)
                                    (figure-out-char s overshoot-detectors (raw-char c)))
                                2))
                      (t (values (setf last nil) nil))))))
    (define-lexable :character (make-detector "#\\"
            (fn (s &optional overshoot-detectors)
                (figure-out-char s overshoot-detectors)))))


(defun define-op (name representation symbol)
    (define-lexable name (make-detector representation (fn (_ &optional _) symbol))))

(define-op :assignment ":=" '|:=|)
(define-op :+= "+=" '+=) (define-op :-= "-=" '-=)
(define-op :*= "*=" '*=) (define-op :/= "/=" '/=)
(define-op :^= "^=" '^=)
(define-op :&= "&=" '&=) (define-op :or= "|=" '\|=)
(define-op :let "=" '=); Binding a value (temporarily) to a value.
(define-op :+ "+" '+) (define-op :++ "++" '++)
(define-op :- "-" '-) (define-op :-- "--" '--)
(define-op :* "*" '*)
(define-op :/ "/" '/)
(define-op :mod "%" 'mod)
(define-op :^ "^" '^)
(define-op :! "!" 'not);    Having the output symbol for these three as NOT, AND
(define-op :AND "&&" 'and); and OR makes them aliases if they get picked up as normal
(define-op :OR "||" 'or);   symbols (but not part of other symbols). Clever, right?
(define-op :equal "==" '==); equality, a comparison test similar to equal
(define-op :<  "<" '<)
(define-op :>  ">" '>)
(define-op :<= "<=" '<=)
(define-op :>= ">=" '>=)
(define-op :<> "<>" '<>)
(define-op :!= "!=" '<>); Another operator for not equal.
(define-op :function "#'" 'function)
(define-op :quote "'" 'quote)
(define-op :funcall "#!" 'call);; An easy way to funcall a function.

; (detectors)
; (detector :+)

(defun close-char (starter-char)
    (case starter-char
        ((#\Space #\Tab #\Newline) #\Newline)
        (#\( #\))
        (#\{ #\})
        (t nil)))

#|
(winner-then-read "3+4" (detectors))

(mapcar (fn (p) (with-input-from-string (in "3+4")
            (list p (iferror (winning-detector in (list (detector p))) 'fail))))
    (mapcar #'detector-name (detectors)))
|#

(defun make-open-parenthesis-accumulator (&optional (closer-char #|(|# #\) ))
    "Make an accumulator for mathsy stuff between parentheses."
    (fn (stream &optional overshoot-detectors)
        "The overshoot-detectors do not need to return accumulator functions like others do."
        (loop with ender = (make-detector closer-char)
               and detectors = (detectors)
               and output = '()
               and unused-chars = '()
              for next-char = (escaping-read-char stream)
            ;; Loop until we find an ender character. After checking, if it's not the end, unread the char.
            until (or (when (null next-char)
                        (cerror "Continue to return anyway (last unused character is ~@C)."
                                "Unexpectedly reached end of input."
                            (car unused-chars))
                        t)
                      (funcall ender next-char)
                      (and (prog1 t (escaping-unread-char next-char stream))
                           overshoot-detectors;; We have 'overshotten' the end of the maths.
                           (winning-detector stream overshoot-detectors t)))
            ;; This will be a new element in our list
            for accumulator = (winning-detector stream detectors)
            ;; Using escaping-read-char above means that you can escape spaces etc. in symbols names.
            do (let ((*read-eval* nil))
                (cond
                    (accumulator
                        (when unused-chars; Push any symbol we were making into output.
                            (push (read-from-string (convert-identifier unused-chars) nil nil) output)
                            (setf unused-chars '())))
                    ((member next-char '(#\Space #\Tab #\Newline))
                        (when unused-chars; Push any symbol we were making into output.
                            (push (read-from-string (convert-identifier unused-chars) nil nil) output)
                            (setf unused-chars '()))
                        (escaping-read-char stream))
                    (t (push (raw-char (escaping-read-char stream)) unused-chars))))
            when accumulator
            do (push (funcall accumulator stream (list ender)) output)
            finally (progn
                (when unused-chars
                    (let ((*read-eval* nil))
                        (push (read-from-string (convert-identifier unused-chars)) output)))
                (return (nreverse output))))))

;; These next two use sub-expression parsing - they return a function call with math args as a list
(let ((args-reader (make-open-parenthesis-accumulator #|(|# #\))))
    (define-lexable :vector (make-detector "#(" #|)|#
            (fn (s &optional _) (list 'vector (funcall args-reader s))))))

(let ((args-reader (make-open-parenthesis-accumulator #\|)))
    (define-lexable :absolute (make-detector #\|
            (fn (s &optional _) (list 'abs (funcall args-reader s))))))

(let ((aref-symbol    (gensym "AREF-"))
      (gethash-symbol (gensym "GETHASH-"))
      (char-symbol    (gensym "CHAR-"))
      (interef-symbol (gensym "INTEREF-"))
      (args-reader (make-open-parenthesis-accumulator #\])))

    (define-lexable :aref-indexes (make-detector #\[
            (fn (s &optional _) `(,aref-symbol ,(funcall args-reader s)))))
    (defun arefp (maths) "Tells if the next object in the maths is an aref-indexes."
        (and (consp maths) (consp (car maths)) (eq (caar maths) aref-symbol)))

    (define-lexable :gethash-key (make-detector "#["
            (fn (s &optional _) `(,gethash-symbol ,(funcall args-reader s)))))
    (defun gethashp (maths) "Tells if the next object in the maths is a gethash-key."
        (and (consp maths) (consp (car maths)) (eq (caar maths) gethash-symbol)))

    (define-lexable :char-index (make-detector "@["
            (fn (s &optional _) `(,char-symbol ,(funcall args-reader s)))))
    (defun charp (maths) "Tells if the next object in the maths is a char-index."
        (and (consp maths) (consp (car maths)) (eq (caar maths) char-symbol)))

    (define-lexable :interpolating-indexes (make-detector "%["
            (fn (s &optional _) `(,interef-symbol ,(funcall args-reader s)))))
    (defun interefp (maths) "Tells if the next object in the maths is an interpolating-indexes."
        (and (consp maths) (consp (car maths)) (eq (caar maths) interef-symbol))))

(defun indexerp (maths) "Tells if the next object in the maths is an indexer."
    (or (arefp maths) (gethashp maths) (charp maths) (interefp maths)))


(define-lexable :open-parenthesis; This one's a bit tricky.
    (make-detector '(#\( #|)|# #\{)
        (fn (char) (make-open-parenthesis-accumulator (close-char char)))))

(defun parse-assignment (maths) "Placeholder for circularity" (declare (ignore maths)))

(defun prognify (forms) "Wraps forms in a progn if there are multiple forms."
    (if (cdr forms) `(progn ,@forms) (car forms)))

(let ((lisp-symbol (gensym "LISP-"));; Unique lisp symbol
      (read-eval-symbol (gensym "READ-EVAL-NEXT-")))

    (define-lexable :lisp
        (make-detector "#L" (fn (stream &optional _) `(,lisp-symbol ,(read stream)))))
    (define-lexable :read-eval (make-detector "#." (fn (_ &optional _) read-eval-symbol)))

    (defun lispp (maths) "Tells if the next object in the maths is a lisp s-expression."
        (and (consp maths) (consp (car maths)) (eq (caar maths) lisp-symbol)))

    (defun eval-read-eval-code (code) "Evaluates one maths after the read-eval symbol. Respects *read-eval*."
        (if (listp code)
            (aif (position read-eval-symbol code)
                (if *read-eval*
                    (multiple-value-bind (to-eval todo) (parse-assignment (subseq code (1+ it)))
                        (append (subseq code 0 it) `((,lisp-symbol ,(eval (prognify to-eval)))) todo))
                    (error "*READ-EVAL* is disabled"))
                (mapcar #'eval-read-eval-code code))
            code)))

(let ((comment-symbol (gensym "COMMENT-")))
    (define-lexable :single-line-comment
        (make-detector #\; (fn (s &optional _) `(,comment-symbol ,(read-line s nil nil t)))))
    (define-lexable :multi-line-comment
        (make-detector "#|" (fn (s &optional _) `(,comment-symbol ,(read-until-string s "|#")))))
    (defun commentp (math) "Tells if the object is a comment."
        (and (consp math) (eq (car math) comment-symbol)))
    (defun remove-comments (maths) "Removes comments from the code"
        (if (listp maths)
            (mapcar #'remove-comments (remove-if #'commentp maths))
            maths)))

(defun read-math (stream c p)
    "The arguments required by the reader dispatch. c is the dispatch sub-character - which
will be M unless you associate this function with another sub-char. p is the integer read
before the sub-character."
    (declare (ignore c p)); Until you or I find a use for them...
    `(mathp ,(eval-read-eval-code
            (let* ((c (read-char stream))
                   (closing-char (or (close-char c) (progn (unread-char c) #\Newline))))
                (funcall (make-open-parenthesis-accumulator closing-char)
                         stream
                         (when (char= closing-char #\Newline)
                            (mapcar #'make-detector '(#|(|# #\) "#M"))))))))


(set-dispatch-macro-character #\# #\M #'read-math)

#|
(list '#M(a+b) '#M(3.1415926) '#M[3-(b+[b+a]+a)+4])
(progn '#M a+b*45 - 4*pi^2*sin(x-b))
(list 'practice '#M(3 + \"maths!\") '#M{"howzat"-"chutzpah" ^ "clanger"})
(values 'practice '#M 3 + 4)
(values 'practice '#M 3 + "hoho")
|#








#|;;; Parsing the maths!
The mathp macro is a facade. The real work is done in parser functions.
Each parser function takes tokenised maths as input and consumes from the
beginning of the input. Once it has produced the lisp code corresponding
to the maths object, it returns two values, the lisp code forms (in a list)
and the tokenised maths that still needs to be parsed.

Getting the reader to emit a macro call on the tokenised maths allows users
to view it simply by quoting the #M statement.

If parsing produces no code, the first return value would be an empty list.
If parsing fails, return (values '() original-maths).
|#

(defparameter *functions* (make-hash-table) "Functions with number of arguments checking in maths.")
(defparameter *global-callables* '() "Functions and macros defined by defun and defmacro in the current maths.")
(defparameter *math-macros* (make-hash-table) "Macros defined with custom parsing.")
(defparameter *local-functions* '() "Local functions in the maths")

(defun define-function (name minargs &optional maxargs)
    "Allows you to specify min & max numbers of arguments for functions used in MathP."
    (setf (gethash name *functions*) (list minargs maxargs)) name)
(defun function-nargs (name)
    "Returns the argument limits for the function. If the function is not specifically
defined as a maths function, there are no argument limits."
    (gethash name *functions* '(0)))

(mapc (lambda (f) (define-function f 1 1))
    '(sin  cos  tan  asin  acos  exp   random  sqrt numerator   rational
      sinh cosh tanh asinh acosh atanh signum isqrt denominator rationalize
      realpart imagpart cis  conjugate phase
      integer-length abs))
(mapc (lambda (args) (apply #'define-function args))
    '((ash 2 2) (atan 1 2) (byte 2 2) (byte-position 1 1) (byte-size 1 1)
      (ceiling 1 2) (complex 1 2) (deposit-field 3 3) (dpb 3 3) (expt 2 2)
      (float 1 2) (floor 1 2) (gcd 0) (lcm 0) (ldb 2 2) (log 1 2)
      (logand 0) (logandc1 2 2) (logandc2 2 2) (logcount 1 1) (logeqv 0)
      (logior 0) (lognand 2 2) (lognor 2 2) (lognot 1 1) (logorc1 2 2)
      (logorc2 2 2) (logxor 0) (mask-field 2 2) (max 1) (min 1) (mod 2 2) (rem 2 2)
      (round 1 2) (truncate 1 2) (values 0)))

(defun is-function (maths) "Predicate for whether next up in maths is a maths function."
    (and (symbolp (car maths))
         (or (member  (car maths) *local-functions*)
             (fboundp (car maths))
             (member  (car maths) *global-callables*))
         (not (gethash (car maths) *math-macros*))
         (listp (cadr maths)))); Functions must have an argument list next.

(defun is-math-macro (maths) "Predicate for whether next up in maths is a maths macro."
    (and (symbolp (car maths)) (gethash (car maths) *math-macros*)))
(defun define-math-macro (name parser)
    (setf (gethash name *math-macros*) parser))

(defun parse-multiple-assignments (maths) "Placeholder for circularity" (declare (ignore maths)))
(defun parse-multiple-maths (maths) "Placeholder for circularity" (declare (ignore maths)))

(defun parse-argument-list (maths) "Parses an argument list from the front of maths."
    (let* ((args (car maths))
           (args (if (listp args) args (list args)))
           (args (mapcar (fn (arg) (if (listp arg) (parse-multiple-maths arg) arg)) args)))
        (values args (cdr maths))))

(defun parse-function (maths)
    (let ((func (car maths))
          (args (parse-multiple-assignments (cadr maths)))
          (todo (cddr maths)))
        (cond ((is-function maths)
                (let ((arg-limits (function-nargs func)))
                    (unless (>= (length args) (car arg-limits))
                        (error "Not enough arguments to maths function ~A. Expected at least ~A arguments."
                               func (car arg-limits)))
                    (unless (or (null (cadr arg-limits)) (<= (length args) (cadr arg-limits)))
                        (error "Too many arguments to maths function ~A. Expected at most ~A arguments."
                               func (cadr arg-limits))))
                (values (list (cons func args)) todo))
              (t (cerror "Continue and return nothing for ~S~S."
                    "Attempted to parse non-function as function." func args)
                 (values '() maths)))))

(defun parse-math-macro (maths) "Parse a maths macro. Mostly just dispatches to the math-macro's parser."
    (aif (gethash (car maths) *math-macros*)
        (funcall it (cdr maths))
        (values '() maths)))

(defun parse-name (maths) "Parse a place for binding in a let or similar"
    (if (symbolp (car maths));; Check it's not an operator symbol?
        (values (list (car maths)) (cdr maths))
        (values '() maths)))

(defun parse-names (maths) "Parses multiple names in a sublist and returns the name list."
    (let ((next (car maths)) (todo (cdr maths)))
        (cond ((null next)    (values '() todo))
              ((symbolp next) (values (list next) todo))
              ((and (consp next) (every #'symbolp next)) (values next todo))
              (t (values '() maths)))))

(define-math-macro 'fn (fn (maths)
        (multiple-value-bind (args todo) (parse-argument-list maths);; Function arguments
            (multiple-value-bind (body todo) (parse-assignment todo)
                (values `((fn ,args ,@body)) todo)))))

(define-math-macro 'function (fn (maths)
        (values `((function ,(car maths))) (cdr maths))))

(define-math-macro 'if (fn (maths)
        (multiple-value-bind (test todo1) (parse-assignment maths)
            (multiple-value-bind (then todo2) (parse-assignment todo1)
                (multiple-value-bind (else todo3) (parse-assignment todo2)
                    (when (or (eq maths todo1) (eq todo1 todo2) (eq todo2 todo3))
                        (error "Invalid IF expression. Arguments must be three assignment forms for the test and the two branches."))
                    (values `((if ,(prognify test) ,(prognify then) ,(prognify else))) todo3))))))

(define-math-macro 'defun (fn (maths)
        (multiple-value-bind (args todo) (parse-argument-list (cdr maths));; Skip name
            (push (car maths) *global-callables*);; Register the function (for recursion and later)
            (multiple-value-bind (body todo) (parse-assignment todo)
                (values `((defun ,(car maths) ,args ,@body)) todo)))))

(define-math-macro 'defmacro (fn (maths)
        (multiple-value-bind (args todo) (parse-argument-list (cdr maths));; Skip name
            (push (car maths) *global-callables*);; Register the macro (for recursion and later)
            (multiple-value-bind (body todo) (parse-assignment todo)
                (values `((defmacro ,(car maths) ,args ,@body)) todo)))))

(define-math-macro 'call (fn (maths)
        (multiple-value-bind (func todo) (parse-assignment maths)
            (multiple-value-bind (args todo2) (parse-multiple-assignments (car todo))
                (when todo2 (error "Unparsed arguments to ~A." func))
                (values `((funcall ,(prognify func) ,@args)) (cdr todo))))))

(defun parse-place (maths) "Parses a setfable place (i.e. can be set with :=)"
    (let ((next (car maths)) (todo (cdr maths)))
        (cond
            ((null maths) (error "Expected place, but there's nothing left."))
            ((member next '(|:=| = ^ * /));; Done before is-function 'cos most of these are functions, but are specially handled in maths!
                (values (list next) todo))
            ((is-math-macro maths) (parse-math-macro maths))
            ((is-function maths) (parse-function maths))
            ((symbolp next) (values (list next) todo))
            ((lispp maths)  (values (list (cadr next)) todo))
            ((consp (car maths))
                (multiple-value-bind (sub-maths unused) (parse-multiple-maths next)
                    (when unused (error "Problem while parsing sub-brackets: ~S." next))
                    (values sub-maths todo)))
            (t (error "Expected place, but got ~S." maths)))))

(define-math-macro '++ (fn (maths)
        (multiple-value-bind (place todo) (parse-place maths)
            (values `((incf ,(prognify place))) todo))))

(define-math-macro '-- (fn (maths)
        (multiple-value-bind (place todo) (parse-place maths)
            (values `((decf ,(prognify place))) todo))))

(defun placep (maths) "Tells if the next object in the maths is a place"
    (or (and (consp maths) (car maths) (or (symbolp (car maths))
                                           (indexerp (cdr maths))))
        ;; Indexing is allowed on sub-expressions, but not on arbitrary maths.
        (lispp maths)))

(defun parse-indexed (maths) "Parses indexed accessors, including aref, gethash, char and an interpolating aref."
    (multiple-value-bind (original todo) (parse-place maths)
        (loop with form = original
            while (cond
                ((arefp todo)
                    (setf form `((aref ,(prognify form) ,@(parse-multiple-assignments (cadar todo))))))
                ((gethashp todo)
                    (setf form `((gethash ,(prognify (parse-multiple-assignments (cadar todo))) ,(prognify form)))))
                ((charp todo)
                    (setf form `((char ,(prognify form) ,(prognify (parse-multiple-assignments (cadar todo)))))))
                ((interefp todo)
                    (setf form `((interpolating-aref ,(prognify form) ,@(parse-multiple-assignments (cadar todo)))))))
            do (setf todo (cdr todo))
            finally (return (values form todo)))))

(labels ((helper (array upto subscripts)
            (cond ((>= upto (length subscripts))    (apply #'aref array subscripts))
                  ((integerp (elt subscripts upto)) (helper array (1+ upto) subscripts))
                  (t (let* ((i (elt subscripts upto))
                            (low-i (floor i))
                            (low-val (helper array (1+ upto)
                                     (append (subseq subscripts 0 upto)
                                             (list* low-i (subseq subscripts (1+ upto))))))
                            (hih-val (helper array (1+ upto)
                                     (append (subseq subscripts 0 upto)
                                             (list* (1+ low-i) (subseq subscripts (1+ upto)))))))
                        (+ low-val (* (- i low-i) (- hih-val low-val))))))))
    (defun interpolating-aref (array &rest subscripts)
        (helper array 0 subscripts)))

#|
(interpolating-aref #2A((1 2)(3 4)) 0 0)
(interpolating-aref #2A((1 2)(3 4)) 0 1)
(interpolating-aref #2A((1 2)(3 4)) 1 0)
(interpolating-aref #2A((1 2)(3 4)) 0.5 0.5)
(interpolating-aref #2A((10 20)(30 40)) 1/10 7/10)
|#

(defun make-repeated-parser (element-parser parser-predicate results-constructor)
    "Element-parser must be a parser, parser-predicate must take maths and return non-nil
when parsing should continue, and results-constructor must take three arguments:
the list of collected elements, the remaining maths, and the original maths."
    (lambda (maths) "Created with make-repeated-parser."
        (loop with remaining = maths
               and output = '()
            while (funcall parser-predicate remaining)
            do (multiple-value-bind (lisp todo) (funcall element-parser remaining)
                (push lisp output)
                (when (equal remaining todo)
                    (error "Parsing is not progressing - cannot parse ~S" todo))
                (setf remaining todo))
            finally (return (funcall results-constructor
                                     (nreverse output) remaining maths)))))

(let ((places-parser (make-repeated-parser #'parse-indexed #'placep
                (lambda (out remain maths)
                    (if remain (values '() maths)
                               (values (apply #'append out) nil))))))
    (defun parse-place+ (maths)
        (if (placep maths)
            (parse-indexed maths)
            (values (funcall places-parser (car maths)) (cdr maths)))))

(defun parse-expression (maths) "Parses an expression, including function application, symbols, numbers and brackets."
    (let ((next (car maths)) (todo (cdr maths)))
        (cond ((null maths) (error "Expected expression, but there's nothing left."))
              ((placep maths) (parse-indexed maths)); Parses a place with optional indexing.
              ((atom next) (values (list next) todo))
              ((consp next);; Uses parse-multiple-maths because body code relies on this too.
                (multiple-value-bind (sub-maths unused) (parse-multiple-maths next)
                    (when unused (error "Problem while parsing sub-brackets: ~S." next))
                    (values sub-maths todo)))
              (t (error "Unexpected input: ~S" next)))))

(defun parse-sign (maths) "Placeholder for circularity" (declare (ignore maths)))

(defun parse-exponent (maths) "Parses one exponentiation."
    (multiple-value-bind (next todo) (parse-expression maths)
        (if (eq (car todo) '^)
            (multiple-value-bind (power leftovers) (parse-sign (cdr todo))
                (values
                    (if (and (not (cdr power)) (equalp (car (last power)) 2))
                        `((square ,(prognify next)))
                        `((expt ,(prognify next) ,(prognify power))))
                    leftovers))
            (values next todo))))

(defmacro define-repeated-binary-op (parser-name operator sub-parser documentation)
    "Defines a repeated binary operation parser for OPERATOR.
Sub-expressions are parsed with sub-parser.
If no operator is found, the (sub)parsed expression is returned 'as is'."
    (assert (symbolp parser-name))
    (assert (symbolp operator)); May possibly make optional 2 values: to-parse and to-generate.
    (assert (symbolp sub-parser))
    (assert (stringp documentation))
    `(defun ,parser-name (maths) ,@(aif documentation (list it))
        (multiple-value-bind (first todo) (,sub-parser maths)
            (loop with output = (list (prognify first))
                  while (eq (car todo) ',operator)
                do (multiple-value-bind (next leftovers) (,sub-parser (cdr todo))
                    (when (or (cdr next) (cdr first))
                        (error ,(format nil "Unexpected multiple values found while parsing ~S expression from ~~S."
                                        operator)
;; You need to think hard about whether this makes sense, and if each of the multiple
;; values should be included in the expression built by this. In such a case, you
;; should not prognify here, but instead apply append to the reversed output.
                               maths))
                    (push (prognify next) output)
                    (setf todo leftovers))
                finally (return (values
                        (if (cdr output)
                            (list (list* ',operator (nreverse output)))
                            first)
                        todo))))))

(defun parse-sign (maths) "Parses unary + and - signs"
    (cond ((eq (car maths) '+) (parse-sign (cdr maths)))
          ((eq (car maths) '-)
            (multiple-value-bind (next todo) (parse-sign (cdr maths))
                (let ((next (prognify next)))
                    (values (if (numberp next) `(,(- next)) `((- ,next))) todo))))
          (t (parse-exponent maths))))

(define-repeated-binary-op parse-divide   / parse-sign     "Parses repeated division")
(define-repeated-binary-op parse-times    * parse-divide   "Parses repeated multiplication")
(define-repeated-binary-op parse-subtract - parse-times    "Parses repeated subtraction")
(define-repeated-binary-op parse-addition + parse-subtract "Parses repeated addition")

(defun parse-modulo (maths) "Parses one modulo."
    (multiple-value-bind (next todo) (parse-addition maths)
        (if (eq (car todo) 'mod)
            (multiple-value-bind (modulus leftovers) (parse-addition (cdr todo))
                (values `((mod ,(prognify next) ,(prognify modulus))) leftovers))
            (values next todo))))

(defun math-equal (object1 object2 &rest more-objects)
    "Compares each successive pair of objects for equality."
    (loop for objectn in (cons object2 more-objects)
        do (unless (equalp object1 objectn) (return nil))
        finally (return t)))
(defun not-math-equal (object1 object2 &rest more-objects)
    "Compares each successive pair of objects for inequality.
Note: it doesn't do all comparisons as /= does."
    (loop for a = object1 then b
          for b in (cons object2 more-objects)
        do (when (equalp a b) (return nil))
        finally (return t)))

(defun comparison-name (symbol)
    "Returns the name of the comparison function, or nil if the symbol is not a comparison."
    (case symbol (== 'math-equal)
                 (<> 'not-math-equal)
                 ((< > <= >=) symbol)
                 (t nil))); returning nil means it is not a comparison symbol.

(defun parse-comparisons (maths) "Parses repeated comparisons"
    (multiple-value-bind (first todo) (parse-modulo maths)
        (loop with terms = (list (prognify first))
               and comparisons = '()
              for comparison-name = (comparison-name (car todo))
              while comparison-name
            do (multiple-value-bind (next leftovers) (parse-modulo (cdr todo))
                (when (or (cdr next) (cdr first))
                    (error "Unexpected multiple values ~S found while parsing comparison sub-expression from ~S."
                           next maths))
                (push (prognify next) terms)
                (push comparison-name comparisons)
                (setf todo leftovers))
            finally (return (values
                    (cond
                        ((null (cdr terms)) first)
                        ((every (fn (x) (eq x (car comparisons))) (cdr comparisons))
                            `((,(car comparisons) ,@(nreverse terms))))
                        (t (let* ((bindings '())
                                  (used (list (car terms))))
                                (loop for term in (cdr terms)
                                    do (if (numberp term) (push term used)
                                        (let ((gensym (gensym "COMPARE-")))
                                            (push gensym used) (push (list gensym term) bindings))))
                                `((let ,bindings
                                        (and ,@(loop for comp in (reverse comparisons)
                                                     and g1 in used and g2 in (cdr used)
                                                collect `(,comp ,g1 ,g2))))))))
                    todo)))))

(defun parse-not (maths) "Parses logical negation"
    (if (eq (car maths) 'not)
        (multiple-value-bind (next todo) (parse-not (cdr maths))
            (values `((not ,(prognify next))) todo))
        (parse-comparisons maths)))

(define-repeated-binary-op parse-and and parse-not "Parses repeated and (&&) statements"); parse-comparisons
(define-repeated-binary-op parse-or  or  parse-and "Parses repeated or (||) operations")

(defun mutator-op (?=) (ecase ?= (*= '*) (/= '/) (^= 'expt) (&= 'and) (\|= 'or)))

(defun parse-assignment (maths)
    "Could be called parse-setf, or parse-colon-equal. A := is translated into a setf."
    (multiple-value-bind (places todo1) (parse-place+ maths)
        (if (and places (member (car todo1) '(|:=| += -= *= /= ^= &= \|=)))
            (multiple-value-bind (value todo) (parse-assignment (cdr todo1)); skip the operator
                (let ((set-value (prognify value))
                      (place (prognify places)))
                    (case (car todo1)
                        (|:=| (values (if (cdr places)
                                    `((setf (values ,@places) ,set-value));; This is how you get multiple parallel assignment.
                                    `((setf          ,place   ,set-value)));; One place setf
                                todo))
                        (+= (values `((incf ,place ,set-value)) todo))
                        (-= (values `((decf ,place ,set-value)) todo))
                        (t (values `((setf ,place (,(mutator-op (car todo1)) ,place ,set-value))) todo)))))
            (parse-or maths))))

(let ((assignments-parser (make-repeated-parser #'parse-assignment #'identity
                (fn (out _ _) (values (apply #'append out) nil)))))
    (defun parse-multiple-assignments (maths) (funcall assignments-parser maths)))

(defun names-declaration-p (maths) "Tells if the next object in the maths is a valid list of names (including one name)"
    (and (consp maths)
         (car maths)
         (or (symbolp (car maths))
             (and (listp (car maths)) (every #'symbolp (car maths))))
         (eq (cadr maths) '=)))

(defun function-declaration-p (maths) "Tells whether the next thing in maths matches a function declaration."
    (and (car maths)
         (symbolp (car maths))
         (or (symbolp (cadr maths)) (listp (cadr maths)))
         (eq (caddr maths) '=)))

(defun parse-bindings (maths) "Parses temporary bindings with = and setfs with :=, outputs a let form."
    (cond ((names-declaration-p maths);; One or more names with an = sign to the right.
            (multiple-value-bind (names todo) (parse-names maths);; Just symbols.
                (multiple-value-bind (value todo) (parse-assignment (cdr todo)); skip the =
                        ;; Got the value(s) for the name(s), get the body:
                        (multiple-value-bind (body todo) (parse-multiple-maths todo)
                            (values `((,@(if (cdr names)
                                            `(multiple-value-bind ,names ,(prognify value))
                                            `(let ((,@names ,(prognify value)))))
                                        ,@body))
                                    todo)))))
          ((function-declaration-p maths)
            (multiple-value-bind (names todo) (parse-names (cdr maths))
                (let ((*local-functions* (cons (car maths) *local-functions*)))
                    (multiple-value-bind (func-body todo) (parse-assignment (cdr todo)); skip the =
                        ;; Got the function, get the body:
                        (multiple-value-bind (body todo) (parse-multiple-maths todo)
                            (values `((labels ((,(car maths) ,names ,@func-body)) ,@body))
                                    todo))))))
          (t (parse-assignment maths))))

(let ((maths-parser (make-repeated-parser #'parse-bindings #'identity
                (fn (out _ _) (values (apply #'append out) nil)))))
    (defun parse-multiple-maths (maths) (funcall maths-parser maths)))

(defmacro mathp (maths) "Magic maths!"
    (let ((*global-callables* '()))
        (prognify (parse-multiple-maths (remove-comments maths)))))


;;;; Macro support
(defvar *backquote* (gentemp "`"))
(defvar *comma*     (gentemp ","))
(defvar *comma@*    (gentemp ",@"))
(defvar *comma.*    (gentemp ",."))

;; We may as well use these special symbols instead of gensyms.
(define-op :backquote "`"  *backquote*)
(define-op :comma     ","  *comma*)
(define-op :comma-at  ",@" *comma@*)
(define-op :comma-dot ",." *comma.*)

;; Raise the standard error.
(defun #.*comma@* (form) (declare (ignore form)) (read-from-string ",@"))
(defun #.*comma.* (form) (declare (ignore form)) (read-from-string ",."))
(defun #.*comma* (form) (declare (ignore form)) (read-from-string ","))

(defmacro gen-pprint (name representation)
  `(locally (declare (special ,name))
     (defun ,name (object) (and (consp object) (eq ,name (first object))))
     (set-pprint-dispatch '(satisfies ,name)
			  #'(lambda (stream input)
			      (format stream "~a~s" ,representation (second input))))))

;; Create our own pretty printing dispatch table.
(defvar *my-pprint*
  (with-standard-io-syntax
    (let ((*print-pprint-dispatch* (copy-pprint-dispatch)))
      (gen-pprint *backquote* "`")
      (gen-pprint *comma* ",")
      (gen-pprint *comma@* ",@")
      (gen-pprint *comma.* ",.")
      *print-pprint-dispatch*)))

;; The quasiquotation implemented with an underlying implementation-dependent system.
(defmacro #.*backquote* (object)
  (with-standard-io-syntax
    (read-from-string
     (write-to-string
      `(,*backquote* ,object) :pretty t :pprint-dispatch *my-pprint*))))

(defmacro expression-wrap (symbol)
    "Do basic wrapping for form building terms ',' ',@' and ',.'"
    `(define-math-macro ,symbol (fn (maths)
            (multiple-value-bind (template todo) (parse-expression maths)
                (values `((,,symbol ,,'(prognify template))) todo)))))

(expression-wrap *backquote*)
(expression-wrap *comma*)
(expression-wrap *comma@*)
(expression-wrap *comma.*)

;; Has a special case for immediately quoted expressions, so that if you
;;  type 'a[4] it expands into '(aref a 4) and doesn't cause an error. It
;;  won't pick up on nested terms like that though, and may 'leak' gensyms.
;;  That is: '(5 a[4]) does NOT give '(5 (aref a 4)).
(define-math-macro 'quote (fn (maths)
        (if (consp (car maths))
            (values `(',(car maths)) (cdr maths))
            (multiple-value-bind (template todo) (parse-expression maths)
                (values `(',(prognify template)) todo)))))


;;;; Testing
#|;; Must be run in the MathP package
(in-package #:mathp)
(flet ((test-math-expansion (lisp &optional (math nil math?))
            (if math?
                (unless (equalp lisp (iferror (macroexpand-1 math) 'fail))
                    (list (list :mismatched lisp (macroexpand-1 math) :from math)))
                (unless (eval lisp)
                    (list (list :returned-non-nil lisp))))))
    (or (apply #'append (mapcar (fn (args) (apply #'test-math-expansion args))
                '(((+ a b c d e f)  #M(a + b + c+d+ e +f))
                  ((- a b c d e f)  #M a - b - c-d- e -f )
                  ((+ a b c d e f (- a b c d e f))  #M(a + b + c+d+ e +f  +  a - b - c-d- e -f))
                  ((* a b c d e f)  #M(a * b * c*d* e *f))
                  ((* z (* a (* b (* c d) e) f) g) #M z*(a*(b*(c*d)*e)*f)*g)
                  ((/ a b c d e f) #M(a / b / c/d/ e /f))
                  ((* a b c d e f (/ a b c d e f))  #M a * b * c*d* e *f  *  a / b / c/d/ e /f)
                  ((* (/ a b c d e f) a b c d e f)  #M a / b / c/d/ e /f  *  a * b * c*d* e *f)
                  ((+ (* a (square x)) (* b x) c)   #M(a*x^2 + b*x + c))
                  ((+ (* up1 (/ up2 down1)) add1 (- sub1 sub2 (* mul1 mul2 mul3) (/ up3 down2)) add2)
                   #M(up1*up2/down1 +add1+ sub1-sub2  -  mul1 * mul2 * mul3-up3/ down2 +add2))
                  ((* a (expt x (expt 2 3))) #M a*x^2^3)
                  ((* (max a b c) (min a b c) (abs (- a b)))
                    #M( max(a b c) * min(a b c) * abs(a - b) ))
                  ((* (max a b c) (min a b c) (abs (- a b)))
                    #M max(a b c) * min(a b c) * abs(a - b))
                  (fail #M( abs() ))
                  ((setf a 4) #M a:=4)
                  ((let ((a (list 1 2 3 4 5))) (setf (cadr a) "B") a)
                    #M a=list(1 2 3 4 5) cadr(a):="B" a)
                  (fail #Mabs(a b c))
                  ((floor (+ a b) c) #M{floor(a+b c)})
                  ((byte-size 10) #M{byte\-size(10)} )
                  ((byte-size 10) #M{byteSize(10)} )
                  ((byte-size 10) #M{ByteSize(10)} )
                  ((= 9 (let ((a 4) (b 60) (c 7)) #M{floor(a+b c)})))
                  ((setf a (setf b (setf c (setf d (setf e (setf f (* 3 6)))))))
                    #M a := b := c := d := e := f := 3*6)
                  ((progn (setf a (setf b (setf c (* 3 6)))) (+ a b c))
                    #M a := b := c := 3*6 a+b+c)
                  ((progn (setf a (setf b (setf c nil))) (setf d (setf e (setf f t))))
                    #M a:=b:=c := nil  d:=e:=f := t)
                  ((values a b c) #M values(a b c))
                  ((progn (setf a 3) (setf b 5) (+ (* a b) (length #(4 5 6))))
                    #M a := 3 b := 5 a*b + #L(length #(4 5 6)))
                  ((or (and a b) c) #M a && b || c)
                  ((- (square 3)) #M -3^2)
                  ((and (plusp 4) (oddp 3)) #M plusp(4) and oddp(3))
                  ((> 4 5) #M 4 > 5)
                  ((> 4 5 6) #M 4 > 5 > 6)
                  (#M 4 < 5 < 6 <= 7)
                  ((< 0 x (+ h 3)) #M 0 < x < h+3)
                  (#M x = 5 h = 4 0 <= x < h+2)
                  (#M !0<>3-3<>4*0)
                  ((not-math-equal 0 (- 3 3) (* 4 0)) #M 0<>3-3<>4*0)
                  ((not (not-math-equal 0 (- 3 3) (* 4 0))) #M !0<>3-3<>4*0)
                  (#M 0==3-3==4*0)
                  ((math-equal 0 (- 3 3) (* 4 0)) #M 0==3-3==4*0)
                  ((expt e (- (square i))) #M e^-i^2)
                  ((expt e (square i)) #M e^+i^2)
                  ((expt e (- (- (square i)))) #M e^- -i^2)
                  (#M{!3<>3<>4})
                  (#M{3<>4<>5})
                  (#M{!3==3==4})
                  (#M{3==3==3})
                  ((not #M ! 3==3==3))
                  (#M ! ! 3==3==3)
                  (#M not ! 3==3==3)
                  (#M ! not 3==3==3)
                  (#M !3==4==5)
                  ((let ((a 5)) (let ((b (setf c 6))) (list a b c)))
                    #M a=5 b=c:=6 list(a b c))
                  ((let ((a 5)) (let ((b (setf c 6))) (setf d 7) (let ((e 8)) (list a b c d e))))
                    #M a=5 b=c:=6 d:=7 e=8 list(a b c d e))
                  ((let ((a 5)) (multiple-value-bind (b c) (values 4 3) (setf (values d e) (values 2 1)) (list a b c d e)))
                    #M a=5 (b c)=values(4 3) (d e):=values(2 1) list(a b c d e))
                  ((let ((a 5)) (multiple-value-bind (b c) (values 4 3) (setf (values d e) (values 2 1)) (list a (+ b b) c d e)))
                    #M a=5 (b c)=values(4 3) (d e):=values(2 1) list(a b+b c d e))
                  ((+ (- 3 (+ b (+ b y a) a)) 4) #M 3-(b+{b+y+a}+a)+4)
                  ((char= #\@ #\@) #M char\=(#\@ #\@))
                  ((char= #\Form-Feed #\Form-Feed) #M char\=(#\formFeed #\formFeed))
                  ((char= #\Form-Feed #\Form-Feed) #M char\={#\formFeed #\formFeed})
                  ((vector 1 2 3 4) #M #(1 2 3 4))
                  ((vector (+ 1 2) (- 3 4)) #M #(1+2 3-4))
                  ((length "hoo \"yeah\"")  #Mlength("hoo \"yeah\""))
                  ((abs (- 4 7)) #M|4-7|)
                  (fail #M|4-7 8-12|)
                  ((fn (a) (+ a 3)) #M fn(a)a+3)
                  ((fn (a) (+ a 3)) #M fn a a+3);; Single function argument is parsed correctly.
                  ((fn (a b) (+ a b)) #M fn(a b)a+b)
                  ((funcall (fn (a) (+ a 3)) 4) #M funcall(fn(a)a+3 4))
                  ((and (consp m) (let ((a (car m))) (or (symbolp a) (and (listp a) (every #'symbolp a)))))
                    #M consp(m) && (a = car(m) symbolp(a) || listp(a) && every(#'symbolp a)))
                  ((and (consp maths) (let ((a (car maths))) (or (symbolp a) (and (listp a) (every #'symbolp a)))))
                    #Mconsp(maths)&&(a=car(maths)symbolp(a)||listp(a)&&every(#'symbolp a)))
                  ((labels ((f (x) (square x))) (mapcar #'f (list 1 2 3 4 5)))
                    #M f(x)=x^2 mapcar(function f list(1 2 3 4 5)))
                  ((equal '(1 4 9 16 25) #M f(x)=x^2 mapcar(function f list(1 2 3 4 5))))
                  ((labels ((f (x) (square x))) (+ (f 3) (f 4)))
                    #M f(x)=x^2 f(3)+f(4))
                  ;; These four require loading of matrix-maths.lisp
                  ((square-matrix 1 2 3 4)         #M squareMatrix{1 2 3 4})
                  ((matrix 3 4 #'*)                #M matrix{3 4 #'*})
                  ((matrix 3 4 (fn (i j) (* i j))) #M matrix{3 4 fn(i j)i*j})
                  ((matrix 2 3 (list 1 2 3 4 5 6)) #M matrix(2 3 list{1 2 3 4 5 6}))
                  ((mod (+ 4 6) 3) #M 4+6 % 3)
                  ((mod (+ 4 6) 3) #M 4+6 mod 3)
                  ((= 1 #M 4+6 % 3))
                  (#M 1==4+6%3)
                  ((< 4 2) #M 4 < #.(4+7 % 3))
                  ((< 4 3) #M 4 < #.8-5)
                  ((< 4 3) #M 4 < #. 8 - 5)
                  ((+ 3 (if t (- 5 1) (- 9 1))) #M 3+if t 5-1 9-1)
                  ((equal 13 #M{a = 5  b = 8; Bumble bee in a comment!
                                a + b}))
                  ((equal 49 (funcall #Mfn(x)x^2 #M7)))
                  ((+ 'a b) #M'a+b)
                  ((append '(1 2 3) '(4 5 6)) #M append('(1 2 3) '(4 5 6)))
                  ((equal '(1 2 3 4 5 6) #M append('(1 2 3) '(4 5 6))))
                  ((list 'a 'b 'c) #M list('a 'b 'c))
                  ((equal '(a b c) #M list('a 'b 'c)))
                  ((aref (vector 1 2 (+ 3 5) 4) 2) #M #(1 2 3+5 4)[2])
                  ((equal 8  #M #(1 2 3+5 4)[2]))
                  ((gethash :a b) #M b#[:a])
                  ((interpolating-aref array 5.5 6.5) #M array%[5.5 6.5])
                  ((equal 19 #M squareMatrix(10 20 30 40)%[1/10 7/10]))
                  ((interpolating-aref (vector 10 50 20 100) (/ 13 10)) #M #(10 50 20 100)%[13/10])
                  ((equal 41 #M #(10 50 20 100)%[13/10]))
                  ((defun add6 (x) (print x) (+ x 6))  #M defun add6 x (print(x) x+6))
                  ((defun default-arg (&key (arg 1d-10)) (expt arg 3))
                    #M defun defaultArg (&key (arg #L1d-10)) arg^3)
                  ((funcall #'+ 4 5) #M call #'+(4 5))
                  ((funcall #'+ 4 5) #M #!#'+(4 5));; A syntax for call/funcall.
                  ((incf px (incf vx ax)) #M px += vx += ax)
                  ((incf (aref p 0) (incf (aref v 0) (aref a 0))) #M p[0] += v[0] += a[0])
                  ((progn (setf ok t) (setf ok (and ok t1)) (setf ok (and ok t2)))
                    #M ok:=t ok&=t1 ok&=t2)
                  ((incf time (setf dt 0.5)) #M time += dt:=0.5)
                  ((decf i) #M--i)
                  ((list (incf i) (decf j)) #M list(++i --j))
                  ((push (funcall colour-xy x y) colours) #M push(#!ColourXY(x y) colours))
                  (`a #M`a)
                  (`a #M`(a)); remember, it's backquoted MathP code, not Lisp Code.
                  (`(progn a b c) #M`(a b c))
                  ((setf alist `(progn 1 2 (- 4 1) ,(+ 4 10) ,(aref a 5) ,@(list 1 2 (+ 1 2))))
                    #M alist := `(1 2 4-1 ,(4+10) ,a[5] ,@list(1 2 1+2)))
                  (``(progn a ,,b c) #M``(a ,,b c))
                  (`(progn 1 2 3) #M`(1 2 3))
                  (`(progn (+ 1 0) 2 3) #M`((1+0) 2 3))
                  ('(#\a #\b #\c) #M'(#\a#\b#\c))
                  (`(progn (setf a ,aval) (setf b ,bval)) #M`(a:=,aval b:=,bval))
                  ))) 'success))
|#

;; Function for Collatz Conjecture
; #M defun tri(n p) {m = if evenp(n) n/2 3*n+1 when(p print(m)) if m<5 m tri(m p)}

;; These comments don't nest...
; (equal 11 #M 3+#|what!|#8)
